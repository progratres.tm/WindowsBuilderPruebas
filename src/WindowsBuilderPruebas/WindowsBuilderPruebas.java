package WindowsBuilderPruebas;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import java.awt.Toolkit;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.SwingConstants;

public class WindowsBuilderPruebas {

	private JFrame frmCalculadora;
	private JTextField numero1;
	private JTextField numero2;
	private JTextField resultado;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) 
	{
		EventQueue.invokeLater(new Runnable() 
		{
			public void run() 
			{
				try {
					WindowsBuilderPruebas window = new WindowsBuilderPruebas();
					window.frmCalculadora.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public WindowsBuilderPruebas() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCalculadora = new JFrame();
		frmCalculadora.setIconImage(Toolkit.getDefaultToolkit().getImage("img\\calculadora.jpg"));
		frmCalculadora.setTitle("Calculadora");
		frmCalculadora.setBounds(100, 100, 450, 300);
		frmCalculadora.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmCalculadora.getContentPane().setLayout(null);
		
		JLabel lblNmero = new JLabel("N\u00FAmero");
		lblNmero.setBounds(86, 76, 46, 14);
		frmCalculadora.getContentPane().add(lblNmero);
		
		numero1 = new JTextField();
		numero1.setHorizontalAlignment(SwingConstants.RIGHT);
		numero1.setBounds(149, 73, 86, 20);
		frmCalculadora.getContentPane().add(numero1);
		numero1.setColumns(10);
		
		JLabel lblOtroNmero = new JLabel("Otro N\u00FAmero");
		lblOtroNmero.setBounds(57, 107, 75, 14);
		frmCalculadora.getContentPane().add(lblOtroNmero);
		
		numero2 = new JTextField();
		numero2.setHorizontalAlignment(SwingConstants.RIGHT);
		numero2.setBounds(149, 104, 86, 20);
		frmCalculadora.getContentPane().add(numero2);
		numero2.setColumns(10);
		
		resultado = new JTextField();
		resultado.setEditable(false);
		resultado.setHorizontalAlignment(SwingConstants.RIGHT);
		resultado.setBounds(149, 135, 86, 20);
		frmCalculadora.getContentPane().add(resultado);
		resultado.setColumns(10);
		
		JLabel lblResultado = new JLabel("Resultado");
		lblResultado.setBounds(67, 132, 65, 14);
		frmCalculadora.getContentPane().add(lblResultado);
		
			
		JButton button = new JButton("+");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					resultado.setText( Integer.toString((Integer.parseInt(numero1.getText()) 
									 + Integer.parseInt(numero2.getText())))) ;				
			}

		});
		button.setFont(new Font("Tahoma", Font.PLAIN, 18));
		button.setBounds(245, 132, 54, 35);
		frmCalculadora.getContentPane().add(button);
		
		JButton button_1 = new JButton("*");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText( Integer.toString((Integer.parseInt(numero1.getText()) 
						 * Integer.parseInt(numero2.getText())))) ;
			}
		});
		button_1.setFont(new Font("Tahoma", Font.PLAIN, 18));
		button_1.setBounds(245, 173, 54, 35);
		frmCalculadora.getContentPane().add(button_1);
		
		JButton btnClear = new JButton("Clear");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resultado.setText( "" ) ;
				numero1.setText( "" ) ;
				numero2.setText( "" ) ;
			}
		});
		btnClear.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnClear.setBounds(309, 133, 75, 33);
		frmCalculadora.getContentPane().add(btnClear);
	}
	
}
